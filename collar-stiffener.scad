/* [Dimensions] */
stiffener_thickness = 0.7; // [0.1:0.1:1]
stiffener_width = 12;      // [1:0.1:30]
stiffener_length = 65;     // [1:0.1:100]
stiffener_tip_radius = 2;  // [0:0.1:10]
stiffener_tip_length = 18; // [0:0.1:30]

/* [Precision] */
$fa = 0.5;
$fs = 0.5;

hull()
{
  // back
  cylinder(d = stiffener_width, h = stiffener_thickness);
  // tip start
  translate([ stiffener_length - stiffener_tip_length, 0, 0 ])
    cylinder(d = stiffener_width, h = stiffener_thickness);
  translate([ stiffener_length, 0, 0 ])
    cylinder(r = stiffener_tip_radius, h = stiffener_thickness);
}
